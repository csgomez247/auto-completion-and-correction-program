Corbin Gomez & Mazar Farran
ECS40

  Automatic Query Correction and Completion Program
*****************************************************
Compiles with 'make'

Run executable with './main'

Type in a word phrase or sentence, press enter, and you'll see possible completions and corrections.

data.txt consists of the first chapter of Mark Twain's "Adventures of Huckleberry Finn"
